#!/usr/bin/env python3
import copy

import tcod

# компоненти yart
from components.engine import Engine
from components.game_map import GameMap
from components.procgen import generate_dungeon
import components.entity_factories as entity_factories

def main() -> None:
    screen_width = 80
    screen_height = 50

    map_width = screen_width
    map_height = screen_height

    # обмеження на розміри й кількість кімнат
    room_max_size = 10
    room_min_size = 6
    max_rooms = 30

    max_monsters_per_room = 2

    # завантаження тайлсету від dwarf fortress
    tileset = tcod.tileset.load_tilesheet(
            "resources/buddy-graphical.png",               # шлях до файлу з тайлсетом
            16, 16,                                        # кількість колонок, рядків у тайлсеті
            tcod.tileset.CHARMAP_CP437                     # таблиця символів (порядок) у тайлсеті (див. док-цію tcod)
            )

    player = copy.deepcopy(entity_factories.player)
    engine = Engine(player = player)

    engine.game_map = generate_dungeon(
            max_rooms = max_rooms,
            room_min_size = room_min_size,
            room_max_size = room_max_size,
            map_width = map_width,
            map_height = map_height,
            max_monsters_per_room = max_monsters_per_room,
            engine = engine
            )
    engine.update_fov()

    with tcod.context.new_terminal(
            screen_width,
            screen_height,
            tileset = tileset,
            title = "Yet Another Roguelike Tutorial",
            vsync = True
            ) as context:
        root_console = tcod.console.Console(screen_width, screen_height, order = "F")
        while True:
            engine.render(console = root_console, context = context)
            engine.event_handler.handle_events()

if __name__ == "__main__":
    main()
