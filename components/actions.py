# components/actions.py
# компонент yart, описує можливі дії з персонажем
from __future__ import annotations
from typing import Optional, Tuple, TYPE_CHECKING

if TYPE_CHECKING:
    from components.engine import Engine
    from components.entity import Entity

class Action:
    def __init__(self, entity: Entity) -> None:
        super().__init__()
        self.entity = entity

    @property
    def engine(self) -> Engine:
        """
        повертає посилання на рушій, до якого відноситься дія
        """
        return self.entity.gamemap.engine

    def perform(self) -> None:
        """
        виконати дію — оскільки це загальний клас, кожен тип дії має перевизначити цей метод під себе.
        - self.engine: контекст для дії (об'єкт класу Engine);
        - self.entity: об'єкт (персонаж чи створіння), що виконують дію.
        """
        raise NotImplementedError()

class EscapeAction(Action):
    def perform(self) -> None:
        raise SystemExit()

class ActionWithDirectrion(Action):
    def __init__(self, entity: Entity, dx: int, dy: int):
        super().__init__(entity)
        self.dx = dx
        self.dy = dy

    @property
    def dest_xy(self) -> Tuple[int, int]:
        """
        повертає координати цільової плитки для дії
        """
        return self.entity.x + self.dx, self.entity.y + self.dy

    @property
    def blocking_entity(self) -> Optional[Entity]:
        """
        повертає об'єкт (entity), що займає цільову плитку
        """
        return self.engine.game_map.get_blocking_entity_at_location(*self.dest_xy)

    def perform(self) -> None:
        raise NotImplementedError()

class MeleeAction(ActionWithDirectrion):
    def perform(self) -> None:
        target = self.blocking_entity
        if not target:
            return                          # нема кого атакувати
        print(f"You kick the {target.name}, much to its annoyance!")

class MovementAction(ActionWithDirectrion):
    def perform(self) -> None:
        dest_x, dest_y = self.dest_xy

        if not self.engine.game_map.in_bounds(dest_x, dest_y):
            return                          # цільова плитка поза межами мапи
        if not self.engine.game_map.tiles["walkable"][dest_x, dest_y]:
            return                          # цільова плитка — не «прохідна»
        if self.engine.game_map.get_blocking_entity_at_location(dest_x, dest_y):
            return                          # цільова плитка зайнята npc

        self.entity.move(self.dx, self.dy)

class BumpAction(ActionWithDirectrion):
    def perform(self) -> None:
        if self.blocking_entity:
            return MeleeAction(self.entity, self.dx, self.dy).perform()
        else:
            return MovementAction(self.entity, self.dx, self.dy).perform()
