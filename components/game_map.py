# compoments/game_map
# компонент yart; зберігає мапу рівня
from __future__ import annotations

from typing import Iterable, Optional, TYPE_CHECKING

import numpy as np
from tcod.console import Console
import components.tiles as tiles

if TYPE_CHECKING:
    from components.engine import Engine
    from components.entity import Entity

class GameMap:
    def __init__(
            self, 
            engine: Engine,
            width: int, 
            height: int, 
            entities: Iterable[Entity] = ()
            ):
        self.engine = engine
        self.width, self.height = width, height
        self.entities = set(entities)
        self.tiles = np.full((width, height), fill_value = tiles.wall, order = "F")
        self.visible = np.full((width, height), fill_value = False, order = "F")
        self.explored = np.full((width, height), fill_value = False, order = "F")

    def get_blocking_entity_at_location(
            self,
            location_x: int,
            location_y: int
            ) -> Optional[Entity]:
        for entity in self.entities:
            if (
                    entity.blocks_movement
                    and entity.x == location_x
                    and entity.y == location_y
                    ):
                return entity
        return None

    def in_bounds(self, x: int, y: int) -> bool:
        """
        повертає True, якщо координати не «випадають» за межі мапи
        """
        return 0 <= x < self.width and 0 <= y < self.height

    def render(self, console: Console) -> None:
        """
        промальовування мапи на екран з урахуванням видимості й пам'яті бачених плиток
        """
        console.rgb[0 : self.width, 0 : self.height] = np.select(
                condlist = [self.visible, self.explored],
                choicelist = [self.tiles["light"], self.tiles["dark"]],
                default = tiles.SHROUD
                )
        for entity in self.entities:
            # відмальовувати лише npc в зоровому полі головного персонажа
            if self.visible[entity.x, entity.y]:
                console.print(x = entity.x, y = entity.y, string = entity.char, fg = entity.color)

