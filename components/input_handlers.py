# components/input_handlers.py
# компонент yart; описує обробник натискань з клавіатури

# системні та додаткові модулі python
from __future__ import annotations
from typing import Optional, TYPE_CHECKING

# бібліотека tcod
import tcod.event

# власні компоненти yart
from components.actions import Action, BumpAction, EscapeAction

if TYPE_CHECKING:
    from components.engine import Engine

class EventHandler(tcod.event.EventDispatch[Action]):
    def __init__(self, engine: Engine):
        self.engine = engine

    def handle_events(self) -> None:
        for event in tcod.event.wait():
            action = self.dispatch(event)

            if action is None:
                continue

            action.perform()

            self.engine.handle_enemy_turns()
            self.engine.update_fov()

    def ev_quit(self, event: tcod.event.Quit) -> Optional[Action]:
        raise SystemExit()

    def ev_keydown(self, event: tcod.event.KeyDown) -> Optional[Action]:
        action: Optional[Action] = None

        key = event.sym
        player = self.engine.player

        if key == tcod.event.KeySym.UP or key == tcod.event.KeySym.k:
            action = BumpAction(player, dx = 0, dy = -1)        # догори
        elif key == tcod.event.KeySym.DOWN or key == tcod.event.KeySym.j:
            action = BumpAction(player, dx = 0, dy = 1)         # додолу
        elif key == tcod.event.KeySym.LEFT or key == tcod.event.KeySym.h:
            action = BumpAction(player, dx = -1, dy = 0)        # ліворуч
        elif key == tcod.event.KeySym.RIGHT or key == tcod.event.KeySym.l:
            action = BumpAction(player, dx = 1, dy = 0)         # праворуч
        elif key == tcod.event.KeySym.y:
            action = BumpAction(player, dx = -1, dy = -1)       # догори-ліворуч
        elif key == tcod.event.KeySym.u:
            action = BumpAction(player, dx = 1, dy = -1)        # догори-праворуч
        elif key == tcod.event.KeySym.b:
            action = BumpAction(player, dx = -1, dy = 1)        # додолу-ліворуч
        elif key == tcod.event.KeySym.n:
            action = BumpAction(player, dx = 1, dy = 1)         # додолу-праворуч
        elif key == tcod.event.KeySym.ESCAPE:
            action = EscapeAction(player)                       # вихід з гри
        # якась зовсім інша клавіша, без прив'язаної дії? повернеться None
        return action
