# components/tiles.py
# компонент yart
from typing import Tuple
import numpy as np

# опис графічного відображення плитки (символ, кольори) як тип,
# сумісний з tcod.Console.tiles_rgb
graphic_dt = np.dtype([
        ("ch", np.int32),           # код символу unicode
        ("fg", "3B"),               # 3 беззнакових байти під колір символу
        ("bg", "3B")                # ...і під колір тла
    ])

# опис властивостей плитки
tile_dt = np.dtype([
        ("walkable", np.bool_),
        ("transparent", np.bool_),
        ("dark", graphic_dt),       # коли плитка не в полі зору
        ("light", graphic_dt)       # коли плитка в полі зору
    ])

def new_tile(
        *,                          # примусове використання іменованих аргументів
        walkable: int,
        transparent: int,
        dark: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]],
        light: Tuple[int, Tuple[int, int, int], Tuple[int, int, int]]
        ) -> np.ndarray:
    """
    функція для кодування властивостей плитки з окремих параметрів
    """
    return np.array((walkable, transparent, dark, light), dtype = tile_dt)

# так відображаються невидимі плитки мапи (недосліджені ділянки)
SHROUD = np.array((ord(" "), (255, 255, 255), (0, 0, 0)), dtype = graphic_dt)

floor = new_tile(
        walkable = True,
        transparent = True,
        dark = (ord(" "), (255, 255, 255), (50, 50, 150)),
        light = (ord(" "), (255, 255, 255), (200, 180, 50))
        )

wall = new_tile(
        walkable = False,
        transparent = False,
        dark = (ord("#"), (255, 255, 255), (0, 0, 100)),
        light = (ord("#"), (255, 255, 255), (130, 110, 50))
        )
